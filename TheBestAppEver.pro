#-------------------------------------------------
#
# Project created by QtCreator 2017-07-16T09:12:30
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TEMPLATE = app

CONFIG(debug, debug|release){
    TARGET = TheBestAppEver_d
    LIBS += -L'../../../work/vr/lastochka/soft/lib' -lAsoundd
} else {
    TARGET = TheBestAppEver
    LIBS += -L'../../../work/vr/lastochka/soft/lib' -lAsound
}

DESTDIR = ../bin

win32{
    LIBS += -L'C:/Program Files (x86)/OpenAL 1.1 SDK/libs/Win64/' -lOpenAL32
    INCLUDEPATH += 'C:/Program Files (x86)/OpenAL 1.1 SDK/include'
}

INCLUDEPATH += ../../../work/vr/lastochka/soft/ASound/include

INCLUDEPATH += include/
VPATH += src/

HEADERS  += $$files(include/*.h)

SOURCES += $$files(src/*.cpp)
