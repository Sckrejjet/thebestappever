#ifndef BADPIG_H
#define BADPIG_H

#include <QPoint>

class QWidget;
class QLabel;
class ASound;

class BadPig
{

public:
    BadPig(QWidget* parent = nullptr);

    QPoint getPos();

    void move(int x, int y);

    QLabel* pigObj() const;

    void process();

    void show();


private:
    QLabel* canvas_;

    int stepX_;
    int stepY_;

    int stopX_;
    int stopY_;
};

#endif // BADPIG_H
