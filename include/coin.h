#ifndef COIN_H
#define COIN_H

#include <QPoint>

class QWidget;
class QLabel;

class Coin
{

public:
    Coin(QWidget* parent);

    QPoint getPos();

    void collected();

    void show();


private:
    QLabel* canvas_;

};

#endif // COIN_H
