#ifndef HERO_H
#define HERO_H

#include <QPoint>

class QWidget;
class QLabel;

class Hero
{

public:
    Hero(QWidget* parent);

    QPoint getPos();

    void setTarget(int x, int y);

    void process();

private:
    QLabel* canvas_;

    double step_;

    double x_;
    double y_;

    int targetX_;
    int targetY_;
};

#endif // HERO_H
