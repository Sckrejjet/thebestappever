#ifndef MAINWIDGET_H
#define MAINWIDGET_H

#include <QWidget>
#include <QTimer>

#include "asound.h"
#include "badpig.h"
#include "hero.h"
#include "coin.h"

class MainWidget : public QWidget
{
    Q_OBJECT

public:
    MainWidget(QWidget *parent = 0);
    ~MainWidget();


public slots:
    void onPigLClick();

    bool eventFilter(QObject *watched, QEvent *event);

private slots:
    void mousePressEvent(QMouseEvent* e);

    void onProcessTimer();

    void mouseMoveEvent(QMouseEvent* e);


private:
    ASound*     musicPlayer_;

    ASound*     gameOver_;

    ASound*     soundCoin_;

    QList<ASound*> listWee_;

    int counter_;

    Hero* hero_;

    BadPig*     badPig_;

    QList<BadPig*> listBadPigs_;

    Coin* coin_;

    QTimer* processTimer_;

    bool intersects(QPoint p1, QPoint p2, int way);
};

#endif // MAINWIDGET_H
