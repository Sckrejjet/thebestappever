#include "badpig.h"

#include "asound.h"

#include <QWidget>
#include <QLabel>
#include <QObject>
#include <QPixmap>

const QString IMG_NAME = QString("../TheBestAppEver/img/badPig.png");

BadPig::BadPig(QWidget *parent)
    : canvas_(new QLabel(parent))
    , stepX_(rand()%4 + 2)
    , stepY_(rand()%4 + 2)
    , stopX_(parent->width() - 50)
    , stopY_(parent->height() - 50)
{
    canvas_->setObjectName("badPig");
    canvas_->resize(50, 50);
    canvas_->setPixmap(QPixmap(IMG_NAME));
}


QPoint BadPig::getPos()
{
    return QPoint(canvas_->x() + 25, canvas_->y() + 25);
}


void BadPig::move(int x, int y)
{
    canvas_->move(x, y);
}



QLabel* BadPig::pigObj() const
{
    return canvas_;
}



void BadPig::process()
{
    canvas_->move(canvas_->x() + stepX_, canvas_->y() + stepY_);

    if (canvas_->y() < 1)
        stepY_ = qrand()%4 + 2;

    if (canvas_->y() > stopY_)
        stepY_ = - (qrand()%4 + 2);

    if (canvas_->x() < 1)
        stepX_ = qrand()%4 + 2;

    if (canvas_->x() > stopX_)
        stepX_ = - (qrand()%4 + 2);
}



void BadPig::show()
{
    canvas_->show();
}
