#include <coin.h>

#include <QWidget>
#include <QLabel>

#include <QImage>
#include <QPixmap>
#include <QPainter>

Coin::Coin(QWidget *parent)
    : canvas_(new QLabel(parent))
{
    canvas_->resize(10, 10);

    QImage img(canvas_->size(), QImage::Format_ARGB32_Premultiplied);
    img.fill(Qt::transparent);

    QPainter p(&img);
    p.setBrush(Qt::yellow);
    p.drawEllipse(0, 0, 10 - 1, 10 - 1);
    p.end();

    canvas_->setPixmap(QPixmap::fromImage(img));

    canvas_->move(qrand()%(parent->width() - 50) + 25,
                  qrand()%(parent->height() - 50) + 25);
}


QPoint Coin::getPos()
{
    return QPoint(canvas_->x() + 5, canvas_->y() + 5);
}



void Coin::collected()
{
    delete canvas_;
}



void Coin::show()
{
    canvas_->show();
}
