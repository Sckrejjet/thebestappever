#include <hero.h>

#include <QWidget>
#include <QLabel>

#include <QImage>
#include <QPixmap>
#include <QPainter>

Hero::Hero(QWidget *parent)
    : canvas_(new QLabel(parent))
    , step_(4.0)
    , x_(0.0)
    , y_(0.0)
    , targetX_(0)
    , targetY_(0)
{
    canvas_->resize(20, 20);

    QImage img(canvas_->size(), QImage::Format_ARGB32_Premultiplied);
    img.fill(Qt::transparent);

    QPainter p(&img);
    p.setBrush(Qt::black);
    p.drawEllipse(0, 0, 20 - 1, 20 - 1);
    p.end();

    canvas_->setPixmap(QPixmap::fromImage(img));
}


QPoint Hero::getPos()
{
    return QPoint(canvas_->x() + 10, canvas_->y() + 10);
}


void Hero::setTarget(int x, int y)
{
    targetX_ = x;
    targetY_ = y;
}


void Hero::process()
{
    int xxx = targetX_ - canvas_->x() - 10;
    int yyy = targetY_ - canvas_->y() - 10;
    double way = sqrt(xxx * xxx + yyy * yyy);

    if (way > 5)
    {
    x_ += xxx/(way / step_);
    y_ += yyy/(way / step_);

        canvas_->move(x_, y_);
    }
}
