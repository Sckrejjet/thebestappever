#include "mainwidget.h"
#include <QMouseEvent>
#include <QLabel>

const QString SOUND_PATH = "../TheBestAppEver/sounds/";

MainWidget::MainWidget(QWidget *parent)
    : QWidget(parent)
    , counter_(0)
{
    this->resize(800, 500);

    AListener listener_ = AListener::getInstance();
    Q_UNUSED(listener_);

    musicPlayer_ = new ASound(SOUND_PATH + "winter.wav", this);
    musicPlayer_->setVolume(30);
    musicPlayer_->setLoop(true);
    musicPlayer_->play();

    for (int i = 0; i < 6; ++i)
        listWee_.append(new ASound(SOUND_PATH + "wee" + QString::number(i) + ".wav"));

    gameOver_ = new ASound(SOUND_PATH + "haha.wav", this);

    soundCoin_ = new ASound(SOUND_PATH + "coin.wav", this);

    badPig_ = new BadPig(this);
    badPig_->pigObj()->installEventFilter(this);

    hero_ = new Hero(this);

    for (int i = 0; i < 1; ++i)
    {
        listBadPigs_.append(new BadPig(this));
        listBadPigs_[i]->move(i * 70, 100);
        listBadPigs_[i]->pigObj()->installEventFilter(this);
    }

    coin_ = new Coin(this);

    processTimer_ = new QTimer(this);

    connect(processTimer_, SIGNAL(timeout()),
            this, SLOT(onProcessTimer()));

    processTimer_->start(20);

    setMouseTracking(true);
}

MainWidget::~MainWidget()
{

}



bool MainWidget::eventFilter(QObject *watched, QEvent *event)
{
    if (watched->objectName() == "badPig" && event->type() == QEvent::MouseButtonPress)
    {
        QMouseEvent* keyEvent = static_cast<QMouseEvent*>(event);
        if (keyEvent->button() == Qt::LeftButton)
        {
            listWee_[qrand()%6]->play();
            return true;
        }
        else
        {
            return false;
        }
    }
    return false;
}



void MainWidget::onPigLClick()
{
    //listWee_[qrand()%3]->play();
}

void MainWidget::mousePressEvent(QMouseEvent *e)
{
//    if (e->button() == Qt::LeftButton)
//    {
//        listWee_[qrand()%3]->play();
//    }
}



void MainWidget::onProcessTimer()
{
    if (counter_ == 300)
    {
        listBadPigs_.append(new BadPig(this));
        listBadPigs_.last()->show();
        listWee_[qrand()%6]->play();
        counter_ = 0;
    }

    hero_->process();

    foreach (BadPig* pig, listBadPigs_)
    {
        pig->process();

        if (intersects(hero_->getPos(), pig->getPos(), 25))
        {
            musicPlayer_->stop();
            gameOver_->play();
            processTimer_->stop();
        }
    }

    if (intersects(hero_->getPos(), coin_->getPos(), 10))
    {
        soundCoin_->play();
        coin_->collected();
        delete coin_;
        coin_ = new Coin(this);
        coin_->show();
    }

    ++counter_;
}



void MainWidget::mouseMoveEvent(QMouseEvent *e)
{
    hero_->setTarget(e->x(), e->y());
}


bool MainWidget::intersects(QPoint p1, QPoint p2, int way)
{
    int xxx = p1.x() - p2.x();
    int yyy = p1.y() - p2.y();
    double wwway = sqrt(xxx * xxx + yyy * yyy);

    if (wwway < way)
    {
        return true;
    }
    return false;
}
